import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },
    {
      path: "/user",
      name: "user",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/user/UserView.vue"),
    },
    {
      path: "/foodQueue",
      name: "foodQueue",
      component: () => import("../views/QueueManagement/QueueView.vue"),
    },
    {
      path: "/serveQueue",
      name: "serveQueue",
      component: () => import("../views/ServingManagement/ServingView.vue"),
    },
    {
      path: "/menu_manage",
      name: "menu_manage",
      component: () => import("../views/MenuManagement/MainMenuView.vue"),
    },
    {
      path: "/clearTable",
      name: "clearTable",
      component: () => import("../views/ClearTable/ClearTableView.vue"),
    },
    {
      path: "/tableStatus",
      name: "tableStatus",
      component: () =>
        import("../views/TableStatusManagement/TableStatusView.vue"),
    },
    {
      path: "/menuAdd",
      name: "menuAdd",
      component: () => import("../views/MenuManagement/MenuAdd.vue"),
    },
    {
      path: "/pointOfSell",
      name: "pointOfSell",
      component: () => import("../views/PointOfSell/PointOfSellView.vue"),
    },
    {
      path: "/TableManagementList",
      name: "TableManagementListe",
      component: () =>
        import("../views/TableManagement/TableManagementList.vue"),
    },
    {
      path: "/ChangeTableStatusList",
      name: "ChangeTableStatusList",
      component: () =>
        import("../views/TableManagement/ChangeTableStatusList.vue"),
    },
    {
      path: "/payment",
      name: "payment",
      component: () => import("../views/Payment/PaymentView.vue"),
    },
  ],
});

export default router;
