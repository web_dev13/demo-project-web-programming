import { ref } from "vue";
import { defineStore } from "pinia";
import type FoodQueue from "@/types/FoodQueue";

export const useFoodQueueStore = defineStore("foodQueue", () => {
  const deleteOrderId = ref(-1);

  const showDeleteDialog = ref(false);

  // Mockup Data
  const foodQueues = ref<FoodQueue[]>([
    {
      orderNumber: 1,
      tableNumber: 1,
      customerName: "Juan S. McQueen",
      foodList: ["Pad See Ew"],
      dateTime: " 5 / 1 / 2566 10:00",
      orderStatus: "Pending",
    },
    {
      orderNumber: 2,
      tableNumber: 2,
      customerName: "Elizabeth M. Traynor",
      foodList: ["Som Tam", "Gai Yang", "Khao Niao"],
      dateTime: " 5 / 1 / 2566 10:05",
      orderStatus: "Pending",
    },
    {
      orderNumber: 3,
      tableNumber: 3,
      customerName: "Ronald S. Lucas",
      foodList: ["Som Tam", "Gai Yang", "Khao Niao", "Laab"],
      dateTime: " 5 / 1 / 2566 10:15",
      orderStatus: "Pending",
    },
  ]);

  const denyQueue = (): void => {
    showDeleteDialog.value = false;
    const index = foodQueues.value.findIndex(
      (item) => item.orderNumber === deleteOrderId.value
    );
    foodQueues.value.splice(index, 1);
  };

  const acceptQueue = (id: number): void => {
    const index = foodQueues.value.findIndex((item) => item.orderNumber === id);
    foodQueues.value[index].orderStatus = "Accepted";
  };

  const queueStatus = (id: number): string => {
    const index = foodQueues.value.findIndex((item) => item.orderNumber === id);
    return foodQueues.value[index].orderStatus;
  };

  const deleteConfirm = (id: number) => {
    showDeleteDialog.value = true;
    deleteOrderId.value = id;
    console.log(deleteOrderId.value);
  };

  return {
    acceptQueue,
    foodQueues,
    denyQueue,
    queueStatus,
    showDeleteDialog,
    deleteConfirm,
    deleteOrderId,
  };
});
