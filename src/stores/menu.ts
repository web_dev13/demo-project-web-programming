import { ref } from "vue";
import { defineStore } from "pinia";
import type Menu from "@/types/Menu";

export const useMenuStore = defineStore("menu", () => {
  const showDeleteDialog = ref(false);
  let menuId = 4;
  // const addNew = ref("")
  const deleteMenuId = ref(-1);
  const editedMenu = ref<Menu>({
    id: -1,
    img: "",
    name: "",
    category: "",
    description: "",
    setSize: "",
    price: 0,
    status: "",
  });
  const menu = ref<Menu[]>([
    {
      id: 1,
      img: "/public/Som-Tam-Pu-Pla-Ra.jpg",
      name: "Som Tam",
      category: "E-san",
      description: "E-san",
      setSize: "Normal",
      price: 40,
      status: "Active",
    },
    {
      id: 2,
      img: "/public/Som-Tam-Kai-Khem.jpg",
      name: "Som Tam",
      category: "E-san",
      description: "E-san",
      setSize: "Normal",
      price: 40,
      status: "Active",
    },
    {
      id: 3,
      img: "/public/Som-Tam-Pu.jpg",
      name: "Som Tam",
      category: "E-san",
      description: "E-san",
      setSize: "Normal",
      price: 40,
      status: "Active",
    },
    {
      id: 4,
      img: "/public/Som-Tam-Thai.jpg",
      name: "Som Tam",
      category: "E-san",
      description: "E-san",
      setSize: "Normal",
      price: 40,
      status: "Active",
    },
    {
      id: 5,
      img: "/public/Som-Tam-Thai.jpg",
      name: "Som Tam",
      category: "E-san",
      description: "E-san",
      setSize: "Normal",
      price: 40,
      status: "Active",
    },
    {
      id: 6,
      img: "/public/Som-Tam-Thai.jpg",
      name: "Som Tam",
      category: "E-san",
      description: "E-san",
      setSize: "Normal",
      price: 40,
      status: "Active",
    },
    {
      id: 7,
      img: "/public/Som-Tam-Thai.jpg",
      name: "Som Tam",
      category: "E-san",
      description: "E-san",
      setSize: "Normal",
      price: 40,
      status: "Active",
    },
    {
      id: 8,
      img: "/public/Som-Tam-Thai.jpg",
      name: "Som Tam",
      category: "E-san",
      description: "E-san",
      setSize: "Normal",
      price: 40,
      status: "Active",
    },
  ]);

  const clear = () => {
    editedMenu.value = {
      id: -1,
      img: "",
      name: "",
      category: "",
      description: "",
      setSize: "",
      price: 0,
      status: "",
    };
  };

  const editMenu = (menu: Menu) => {
    editedMenu.value = { ...menu };
    showDeleteDialog.value = true;
  };

  const saveMenu = () => {
    if (editedMenu.value.id < 0) {
      editedMenu.value.id = menuId++;
      menu.value.push(editedMenu.value);
    } else {
      const index = menu.value.findIndex(
        (item) => item.id === editedMenu.value.id
      );
      menu.value[index] = editedMenu.value;
    }
    showDeleteDialog.value = false;
    clear();
  };

  const deleteMenu = (id: number): void => {
    showDeleteDialog.value = false;
    const index = menu.value.findIndex(
      (item) => item.id === deleteMenuId.value
    );
    menu.value.splice(index, 1);
  };

  const deleteConfirm = (id: number) => {
    showDeleteDialog.value = true;
    deleteMenuId.value = id;
    console.log(deleteMenuId.value);
  };

  return {
    menu,
    deleteMenu,
    deleteConfirm,
    showDeleteDialog,
    saveMenu,
    clear,
    editMenu,
  };
});
