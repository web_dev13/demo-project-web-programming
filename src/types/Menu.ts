export default interface Menu {
  id: number;
  img: string;
  name: string;
  category: string;
  description: string;
  setSize: string;
  price: number;
  status: string;
}
