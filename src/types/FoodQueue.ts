export default interface FoodQueue {
  orderNumber: number;
  tableNumber: number;
  customerName: string;
  foodList: string[];
  //foodList: Array<{ id: number; text: string }>;
  dateTime: string;
  orderStatus: string;
}
